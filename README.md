# Gallery App

# Knowledge and Wxperience Question

1. Retrofit, Recyclerview, OkHttp, Glide, Circle Image,
- Retrofit merupakan library untuk melakukan komunikasi data dengan API.
- Recyclerview digunakan untuk menampilkan data berupa list.
- OkHttp untuk melakukan debug pada logcat bahwa API yg kita call sudah benara atau belum.
- Glide digunakan untuk menampilkan gambar dari url API.
- Circle Image untuk menampilakn image berbentuk bulat.

2. Ya. Disini saya menggunakan MVP, manfaatnya adalah memisahkan antara koding logika dan koding layout. Serta mempermudah jika ingin menambahkan method atau functin baru sehingga lebih terstruktur. Untuk clean code saya rasa code saya ini sudah menggunakan clean code karena memakai pattern MVP.